//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//
var http = require('http');
var req= require("request");
var fs = require('fs');
var SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/audios";
var controller= function(request,response)
{
  Array.prototype.unique=function(a){
    return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
  });
  console.log("url "+request.url);
  switch(true)
  {
    case (request.url.indexOf("/favicon.ico")!=-1):  
      return;
      break;
                          
    case (request.url.indexOf("stt")!=-1):
      
      console.log("recibí petición Speech to Text");
      var speech_to_text = new SpeechToTextV1 ({
        username: '4f9e0f08-10d1-4a9c-bc7a-c6bfa426f6eb',
        password: 'blMwVm70aSxj'
      });
      
      var params = {
        model: 'es-ES_BroadbandModel',
        content_type: 'audio/mp3',
        'interim_results': true,
        'max_alternatives': 3,
        'word_confidence': false,
        
      };
      
      // Create the stream.
      var recognizeStream = speech_to_text.createRecognizeStream(params);
      // Pipe in the audio.
      console.log("url req: "+request.url.substr(5));
      req(request.url.substr(5)).pipe(recognizeStream);
      // Pipe out the transcription to a file.
      recognizeStream.pipe(fs.createWriteStream('transcription.txt'));
      
      // Get strings instead of buffers from 'data' events.
      recognizeStream.setEncoding('utf8');
      // Listen for events.
      recognizeStream.on('close',onClose);
      function onClose()
      {
        console.log("Finished");
        fs.readFile("transcription.txt",function(error,file){
          response.setHeader('Content-Type', 'text/html; charset=UTF-8');
          response.write(file);
          var obj= {url:request.url.substr(5), texto:file.toString()};
          console.log(obj);
          var newarr=obj.texto.trim().split(" ").unique();
          var actualWords;
          MongoClient.connect(url, function(err, db) {
            if(err) throw err;
            db.collection("palabras").find({},{ _id: 0}).toArray(function(err,res){
              if(err) throw err;
              console.log("respuesta");
              console.log(res);
              actualWords=res[0];
              console.log(actualWords);
              db.close();
              var arrActualWords=new Array();
              for (var i in actualWords){
              	arrActualWords.push(actualWords[i]);
              }
              arrActualWords=arrActualWords.unique();
              console.log("arrActualWords");
              console.log(arrActualWords);
              var dbarr={};
              for (var i=0;i<newarr.length;i++){
                dbarr[i]=newarr[i];
              }
              for (var i=0;i<arrActualWords.length;i++){
                dbarr[i]=arrActualWords[i];
              }
              console.log(JSON.stringify(dbarr));
              MongoClient.connect(url, function(err, db) {
                if(err) throw err;
                db.collection("textos").insertOne(obj, function(err, res) {
                  if (err) throw err;
                  console.log("Texto insertado!");
                  db.close();
                });
              });
              MongoClient.connect(url, function(err, db) {
                if(err) throw err;
                db.collection("palabras").remove({},function(err,msj){
                  if(err) throw err;              
                  db.close()
                })
                db.collection("palabras").insertOne(JSON.parse(JSON.stringify(dbarr)), function(err, res) {
                  if (err) throw err;
                  console.log("palabras insertadas!");
                  db.close();
                });
              });
              response.end();
            });
          });
        })
      }
      
      break;
    
    case (request.url=="/"):
      console.log("Sin Parametros");
      fs.readFile('./front/index.html', function (err, html) {
        if (err) 
        {
            throw err; 
        }       
        response.writeHeader(200, {"Content-Type": "text/html"});  
        response.write(html);  
        response.end();
      });
      break;
      
    case (request.url.indexOf("txtlist")!=-1):
      console.log("Recibí petición lista JSON");
      console.log(url);
      MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        console.log("conected");
        db.collection("textos").find({},{ _id: 0, url: 0}).toArray(function(err, res) {
          if (err) throw err;
          response.setHeader('Content-Type', 'text/html; charset=UTF-8');
          response.end(JSON.stringify(res));
          db.close();
        });
      });
      break;
      
    case (request.url.indexOf("wordlist")!=-1):
      console.log("Recibí petición palabras");
      MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        console.log("conected");
        db.collection("palabras").find({},{ _id: 0}).toArray(function(err, res) {
          if (err) throw err;
          response.setHeader('Content-Type', 'text/html; charset=UTF-8');
          var array=new Array();
          res=res[0];
          console.log(JSON.stringify(res));
          for (var i in res){
            array.push(res[i]);
          }
          response.end(array.toString());
          console.log(array.length);
          db.close();
        });
      });
      break;
    
    default:
      fs.readFile("./front"+request.url,function(err, file) {
          if (err){
            response.end("Error en parametros de url");
          };
          response.end(file);
      })
      break;
  }
}
var servidor = http.createServer(controller);
servidor.listen(8080);